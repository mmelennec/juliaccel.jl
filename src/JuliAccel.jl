# Author:    Matthieu Melennec
# Created:   21 September 2022
# Updated:   26 October 2022

module JuliAccel

using Random, Distributions
using PhysicalConstants.CODATA2018
using NaturallyUnitful  # Notation issue: hides c, hbar
using DocStringExtensions

using Reexport
@reexport using Symbolics

include("DiffAlgebra.jl/DiffAlgebra.jl")
@reexport using .DiffAlgebra

include("mad_x_parser.jl")
@reexport using .MADXParser


export Proton, Electron
include("Particles.jl")

export Beam, Bunch
include("Beam.jl")

export get_gamma, get_beta
include("Physics.jl")

export genDistrib
export Gaussian
include("Distrib.jl")

export gen_map
export Drift
include("Elements.jl")

export evolve
include("tracking.jl")

end