module MADXParser

using DocStringExtensions
export parse_madx, MAD_X

"""
$(TYPEDEF)

Outputs of a MAD_X file parsiing.

# Fields
$(FIELDS)
"""
struct MAD_X
    """Beam data"""
    beam::Dict{String, Any}
    """Used sequence name"""
    sequence::Dict{String}
    """List of beamline elements"""
    lattice::Vector
end

function Base.show(mad::MAD_X)
    
    len = 0.0

    # drift, dipole, quadrupole, dipedge
    types = ["drift", "sbend", "quadrupole", "dipedge"]
    nTypes = [0, 0, 0, 0]

    for elem in mad.lattice
        len += parse(Float64, elem["l"])
        for i in 1:size(types)[1]
            if elem["type"] == types[i]
                nTypes[i] += 1
            end
        end
    end

    sign = "*"^70
    info = (
        sign * "\n"
        * "MADX-Parser Information\n"
        * "         length:     $len [m]\n"
        * "      #elements:     $(length(mad.lattice))\n"
        * "            *       #drift:     $(string(nTypes[1]))\n"
        * "            *      #dipole:     $(string(nTypes[2]))\n"
        * "            *  #quadrupole:     $(string(nTypes[3]))\n"
        * "            *     #dipedge:     $(string(nTypes[4]))\n"
        * "           beam:\n"
        * "            *     particle:     "*mad.beam["particle"]*"\n"
        * "            * total energy:     $(mad.beam["energy"]) [GeV]\n"
        * sign * "\n"
    )
    
    print(info)
    return info
end


abstract type MADXParserError <: Exception end

struct MADXInputError <: MADXParserError
    obj
    MADXInputError(obj) = new(obj)
end
Base.showerror(io::IO, e::MADXInputError) = print(io, e.obj)

    
_drift = Dict("name"=>"", "l"=>0.0, "type"=>"drift")
_drift_pattern = r"(.*):drift,(.*)=(.*);"

_quadrupole = Dict("name"=>"", "l"=>0.0, "k1"=>0.0, "type"=>"quadrupole")
_quadrupole_pattern = r"(.*):quadrupole,(.*)=(.*),(.*)=(.*);"
_nQuad = 2*(length(_quadrupole)-2)    # Don't count name and type

_sbend = Dict("name"=>"", "l"=>0.0, "angle"=>0.0, "k1"=>0.0,
             "e1"=>.0, "e2"=>0.0, "type"=>"sbend")
_sbend_pattern = r"(.*):sbend,(.*)=(.*),(.*)=(.*),(.*)=(.*),(.*)=(.*),(.*)=(.*);"
_nDipole = 2*(length(_sbend)-2)       # Don't count name and type

_dipedge = Dict("name"=>"", "h"=>0.0, "e1"=>0.0, "fint"=>0.0,
               "hgap"=>0.0, "tilt"=>0.0, "type"=>"dipedge")
_dipedge_pattern = r"(.*):dipedge,(.*)=(.*),(.*)=(.*),(.*)=(.*),(.*)=(.*),(.*)=(.*);"
_nDipedge = 2*(length(_dipedge)-2)    # Don't count name and type


beam = Dict("energy"=>0.0, "particle"=>"")
_beam_pattern = r"beam,(.*)=(.*),(.*)=(.*);"
_nBeam = 2*length(beam)

_line = Dict("name"=>"", "elem"=>[])
_line_pattern = r"(.*):line=\(+(.*)\);"


_elements = []
_lines = []

lattice = []

sequence = Dict("name"=>"")
_seq_pattern = r"use,sequence=(.*);"


# Inspired from 
# https://github.com/ECP-WarpX/impactx/blob/development/src/python/impactx/MADXParser.py
"""
$(SIGNATURES)

Parses the precised MAD X file and returns a `MAD_X` object.

# Example
```jldoctest

julia> mad_x_file = "path/to/MADX/file.mad"

julia> parse_madx(mad_x_file)
```
"""
function parse_madx(file::String)

    open(file) do f
        nLine = 0

        global _elements = []
        global _lines = []
        global lattice = []

        while ! eof(f)
            nLine += 1
            line = readline(f)

            stripped = replace(line, " "=>"")

            if isempty("line")
                # Ignore empty lines

            elseif line[1] == '!'
                # This is a comment

            elseif occursin("drift", stripped)
                obj = collect(match(_drift_pattern, stripped))
                _drift["name"] = obj[1]
                
                if haskey(_drift, obj[2])
                    _drift[obj[2]] = obj[3]
                else 
                    err = "Drift at Line " * string(nLine) * ": Parameter '" * obj[2] * "' does not exist for drift."
                    throw(MADXInputError(err))
                end
                push!(_elements, copy(_drift))


            elseif occursin("quadrupole", stripped)
                obj = collect(match(_quadrupole_pattern, stripped))
                _quadrupole["name"] = obj[1]
                
                for i in 2:2:_nQuad+1
                    if haskey(_quadrupole, obj[i])
                        _quadrupole[obj[i]] = obj[i+1]
                    else 
                        err = "Quadrupole at Line " * string(nLine) * ": Parameter '" * obj[i] * "' does not exist for quadrupole."
                        throw(MADXInputError(err))
                    end
                end
                push!(_elements, copy(_quadrupole))
            

            elseif occursin("sbend", stripped)
                obj = collect(match(_sbend_pattern, stripped))
                _sbend["name"] = obj[1]
                
                for i in 2:2:_nDipole+1
                    if haskey(_sbend, obj[i])
                        _sbend[obj[i]] = obj[i+1]
                    else 
                        err = "Dipole at Line " * string(nLine) * ": Parameter '" * obj[i] * "' does not exist for Dipole."
                        throw(MADXInputError(err))
                    end
                end
                push!(_elements, copy(_sbend))

            
            elseif occursin("dipedge", stripped)
                obj = collect(match(_dipedge_pattern, stripped))
                _dipedge["name"] = obj[1]
                
                for i in 2:2:_nDipedge+1
                    if haskey(_dipedge, obj[i])
                        _dipedge[obj[i]] = obj[i+1]
                    else 
                        err = "DipEdge at Line " * string(nLine) * ": Parameter '" * obj[i] * "' does not exist for Dipole Edge."
                        throw(MADXInputError(err))
                    end
                end
                push!(_elements, copy(_dipedge))


            elseif occursin("marker", stripped)

            
            elseif occursin("beam", stripped)
                obj = collect(match(_beam_pattern, stripped))

                for i in 1:2:_nBeam
                    if haskey(beam, obj[i])
                        if tryparse(Float64, obj[i+1]) isa Float64
                            beam[obj[i]] = parse(Float64, obj[i+1])
                        else beam[obj[i]] = obj[i+1]
                        end
                    
                    else
                        err = "Beam at Line " * string(nLine) * ": Parameter '" * obj[i] * "' does not exist for Beam."
                        throw(MADXInputError(err))
                    end
                end
            
            
            elseif occursin("line", stripped)
                obj = collect(match(_line_pattern, stripped))
                _line["name"] = obj[1]

                lines = string.(split(obj[2], ","))
                newlines = []

                for l in lines
                    if occursin("*", l)
                        tmp = string.(split(l,"*"))
                        
                        n = 0
                        ll = ""

                        if tryparse(Int64,tmp[1]) isa Int64
                            n = parse(Int64,tmp[1])
                            ll = tmp[2]
                        else
                            n = parse(Int64,tmp[2])
                            ll = tmp[1]
                        end

                        for _ in 1:n
                            push!(newlines, ll)
                        end

                    else
                        push!(newlines, l)
                    end
                end

                _line["elem"] = newlines

                push!(_lines, copy(_line))

            
            elseif occursin("use", stripped) && occursin("sequence", stripped)
                obj = collect(match(_seq_pattern, stripped))
                sequence["name"] = obj[1]


            else throw(MADXInputError("Error at line "*string(nLine)*"."))
            end
        end

        start = sequence["name"]
        lattice = _flatten(start)
    end

    return MAD_X(beam, sequence, lattice)
end


function _flatten(line)

    flat_line = []

    for elem in _elements
        if line == elem["name"]
            return [elem]
        end
    end

    for l in _lines
        if line == l["name"]
            for ll in l["elem"]
                flat = _flatten(ll)
                append!(flat_line, flat)
            end
        end
    end

    return flat_line
end

end