# Author:    Matthieu Melennec
# Created:   21 September 2022
# Updated:   26 October 2022

abstract type Element end
"""
$(SIGNATURES)

Function generating the linear transfer map associated to Element E applied on Beam Beam.

# Examples
```jldoctest

julia> E = Drift(1.0)
Drift(1.0)

julia> B = Beam(Proton(), 100.0)
Beam(Proton(9.382720881604904e8, 1.602176634e-19, "Proton"), 100.0)

julia> gen_map(E, B)
6×6 Matrix{Float64}:
 1.0  1.0  0.0  0.0  0.0  0.0
 0.0  1.0  0.0  0.0  0.0  0.0
 0.0  0.0  1.0  1.0  0.0  0.0
 0.0  0.0  0.0  1.0  0.0  0.0
 0.0  0.0  0.0  0.0  1.0  4.69136e6
 0.0  0.0  0.0  0.0  0.0  1.0
```
"""
function gen_map(E::Element, B::Beam) end

"""
$(TYPEDEF)

Bundles data for a Drift element.

# Fields
$(FIELDS)

"""
struct Drift<:Element
    """Length of the Drift."""
    length
end

function gen_map(Dr::Drift, B::Beam)
    γ = get_gamma(B)
    println(γ)
    β0 = get_beta(γ)

    f = Dr.length / (β0 * γ)^2

    R = [1 Dr.length 0 0 0 0
         0 1 0 0 0 0
         0 0 1 Dr.length 0 0
         0 0 0 1 0 0
         0 0 0 0 1 f
         0 0 0 0 0 1]

    return R
end