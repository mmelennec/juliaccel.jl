function poisson(f) #, vars=Γcoord)

    dq = Differential(Γcoord[1])
    dp = Differential(Γcoord[2])

    dqf = expand_derivatives(dq(f))
    dpf = expand_derivatives(dp(f))

    po = Quant(dqf) * dp - Quant(dpf) * dq

    indices = [i for i in 3:2:length(Γcoord)]
    for i in indices
        dq = Differential(Γcoord[i])
        dp = Differential(Γcoord[i+1])
        
        dqf = expand_derivatives(dq(f))
        dpf = expand_derivatives(dp(f))

        po += Quant(dqf) * dp - Quant(dpf) * dq
    end
    return po

end