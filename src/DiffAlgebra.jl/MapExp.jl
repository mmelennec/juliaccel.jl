function exp_map(O, order=trunc_order)
    
    if order == 0; return Id(); end
    
    exp_O = Id() + O
    for k in 2:order
        exp_O += Quant(1//factorial(big(k))) * O^k
    end
    exp_O = Compo(expand_derivatives ∘ exp_O)
    return exp_O

end

"""
$(SIGNATURES)

Function generating a non-linear mapping exp(:f:) induced by `f`.
If the truncation order is not precised, the order set with `@set_trunc_order` will be used.

# Example
```jldoctest

julia> @set_variables q p
2-element Vector{Num}:
 q
 p

julia> H = .5*q^2 + .5*p^2
0.5(p^2) + 0.5(q^2)

julia> M = gen_nl_map(H, 5)
(::JuliAccel.DiffAlgebra.Compo) (generic function with 2 methods)

julia> M(q)
0.54166666666667q - 0.84166666666667p
```
"""
function gen_nl_map(f::Num, order)
    Lie = poisson(f)
    M_nl = exp_map(Lie, order)
    return M_nl
end

function gen_nl_map(f::Num)
    Lie = poisson(f)
    M_nl = exp_map(Lie)
    return M_nl
end
