using Symbolics: Operator, value, unwrap

Base.@kwdef struct Id <: Operator
    coef = 1
end
(I::Id)(x::Num) = I.coef * x
(I::Id)(v::Vector{Num}) = [I(x) for x in v]

struct Quant <: Operator
    f :: Num
end
(Q::Quant)(x::Num) = Num(Q.f)
(Q::Quant)(v::Vector{Num}) = [Q(x) for x in v]


struct Compo <: Operator
    Cf :: ComposedFunction
end
(C::Compo)(x::Num) = C.Cf(x)
(C::Compo)(v::Vector{Num}) = [C(x) for x in v]


struct Add <: Operator
    O1 :: Operator
    O2 :: Operator
end
(S::Add)(x::Num) = Num(S.O1(x) + S.O2(x))
(S::Add)(v::Vector{Num}) = [S(x) for x in v]


struct Sub <: Operator
    O1 :: Operator
    O2 :: Operator
end
(D::Sub)(x::Num) = Num(D.O1(x) - D.O2(x))
(D::Sub)(v::Vector{Num}) = [D(x) for x in v]


struct Prod <: Operator
    O1 :: Operator
    O2 :: Operator
end
(P::Prod)(x::Num) = Num(P.O1(x) * P.O2(x))
(P::Prod)(v::Vector{Num}) = [P(x) for x in v]


struct Div <: Operator
    O1 :: Operator
    O2 :: Operator
end
(R::Div)(x::Num) = Num(R.O1(x) // R.O2(x))
(R::Div)(v::Vector{Num}) = [R(x) for x in v]

Base.:+(O1::Operator, O2::Operator) = Add(O1, O2)
Base.:-(O1::Operator, O2::Operator) = Sub(O1, O2)
Base.:*(O1::Operator, O2::Operator) = Prod(O1, O2)
Base.:/(O1::Operator, O2::Operator) = Div(O1, O2)

function power(x::Any, n::Integer)
    xk = x
    for k in 2:n
        xk = x ∘ xk
    end
    return Compo(xk)
end
Base.:^(O::Operator, n::Integer) = power(O, n)

Base.:*(Q::Quant, D::Differential) = Prod(Q, Compo(expand_derivatives ∘ D))


"""
$(SIGNATURES)

Evaluation function, computing the trasformation of the coordinates `vals` under the effect of an Operator `O`.

# Example
```jldoctest

julia> @set_variables q p
2-element Vector{Num}:
 q
 p

julia> M = gen_nl_map(q^2 + p, 6)
(::JuliAccel.DiffAlgebra.Compo) (generic function with 2 methods)

julia> init = reshape([1, 0], 2, 1)
2×1 Matrix{Int64}:
 1
 0

julia> out = evaluate(M, init)
2×1 Matrix{Real}:
 0
 1.0
```
"""
function evaluate(O::Operator, vals::Matrix{<:Real})
    map = O(Γcoord)
    shape = size(vals)
    out = Matrix{Float64}(undef, shape[1], shape[2])
    for j in 1:shape[2]
        out[:,j] .= unwrap.(substitute.(map, (Dict(Γcoord .=> vals[:,j]),)))
    end
    return out
end
