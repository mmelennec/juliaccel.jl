function tps_coef(N, v)    
    if v == 1       # End recursion
        return [i for i in 0:N]
    
    else
        dim = convert(Int64, factorial(big(N+v)) // (factorial(N) * factorial(v)))
        M = zeros(dim, v)
        tmp = 1

        for n in 0:N
            # Build M recursively
            l = convert(Int64, factorial(n+v-1) // (factorial(n) * factorial(v-1))-1)
            m = tps_coef(n, v-1)
            M[tmp:tmp+l, 2:v] .= m
            M[tmp:tmp+l, 1] .= n .- sum(m, dims=2)
            tmp += l+1
        
        end
        return M
    end
end

"""
$(SIGNATURES)

Computes the Truncated Power Series expansion of Symbolic expression `f`

# Warning

For this function to work, you must have set the truncation order with macro `@set_trunc_order`.

# Example
```jldoctest

julia> @set_variables x
1-element Vector{Num}:
 x

julia> @set_trunc_order 3
4-element Vector{Int64}:
 0
 1
 2
 3

 julia> tps_expr(sqrt(1+x))
 0.0625(x^3) + 0.5x - 0.125(x^2)
```
"""
function tps_expr(f::Num)

    dim = length(Γcoord)

    l = size(M, 1)

    tps = f.val.coeff

    for i in 2:l
        D = []
        var_prod = 1
        for k in 1:dim
            j = convert(Int64,M[i,k])
            if j != 0
                for i in 1:j
                    Dk = Differential(Γcoord[k])
                    append!(D, [Dk])
                end
                var_prod *= Γcoord[k]^j / factorial(j)
            end
        end 
        D_op = prod(D)
        df = expand_derivatives(D_op(f))
        df_func = build_function(df, [var for var in Γcoord], expression=Val{false})
        coef = convert(Float64, df_func(zeros(dim)))
        tps += coef * var_prod
    end
    return tps

end


"""
$(SIGNATURES)

A function to print polynomial coefficient tables.

# Warning

The truncation order must have been set prior to using this function

# Example
```jldoctest

julia> @set_variables x y
2-element Vector{Num}:
 x
 y

julia> @set_trunc_order 2
6×2 Matrix{Float64}:
 0.0  0.0
 1.0  0.0
 0.0  1.0
 2.0  0.0
 1.0  1.0
 0.0  2.0

julia> H = 3x + y^2
3x + y^2

julia> print_poly(H)
7×3 Matrix{Any}:
  "Coeff"  x  y
 0         0  0
 3         1  0
 0         0  1
 0         2  0
 0         1  1
 1         0  2
```
"""
function print_poly(f::Num)
    shape = size(M)
    out = Matrix{Any}(undef, shape[1]+1, shape[2]+1)
    out[1,1] = "Coeff"
    out[2:shape[1]+1, 2:shape[2]+1] = Int.(M[:,:])
    out[1,2:shape[2]+1] = deepcopy(Γcoord)
    for i in 1:shape[1]
        mono = prod([Γcoord[j]^Int(M[i,j]) for j in 1:shape[2]])
        D = Differential(mono)
        proto_coeff = expand_derivatives(D(f))
        coeff = substitute.(proto_coeff, (Dict(Γcoord .=> [0 for _ in 1:shape[2]]),))[1]
        out[i+1,1] = coeff
    end
    display(out)
end


function pvt_tps_expr(f::Num, order=trunc_order)

    dim = length(Γcoord)

    local M = tps_coef(order, dim)

    l = size(M, 1)

    tps = f.val.coeff

    for i in 2:l
        D = []
        var_prod = 1
        for k in 1:dim
            j = convert(Int64,M[i,k])
            if j != 0
                for i in 1:j
                    Dk = Differential(Γcoord[k])
                    append!(D, [Dk])
                end
                var_prod *= Γcoord[k]^j / factorial(j)
            end
        end 
        D_op = prod(D)
        df = expand_derivatives(D_op(f))
        df_func = build_function(df, [var for var in Γcoord], expression=Val{false})
        coef = convert(Float64, df_func(zeros(dim)))
        tps += coef * var_prod
    end
    return tps

end