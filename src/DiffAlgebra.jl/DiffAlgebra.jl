module DiffAlgebra

export tps_expr, print_poly, gen_nl_map, evaluate, gen_matrix
export Compo, Id, Quant
export @set_variables
export @set_trunc_order

using Reexport
@reexport using Symbolics

using DocStringExtensions
using LinearAlgebra

"""
A macro to set the symbols of your variables

# Example
```jldoctest

julia> @set_variables x
1-element Vector{Num}:
 x

julia> H = x^2 + 2
2 + x^2
```
"""
macro set_variables(xs...)
    Γexpr = :(@variables($(xs...)))
    global Γcoord = eval(Γexpr)
    return esc(Γexpr)
end

"""
A macro to set the truncation order.
It also generates the Taylor coefficients of a Truncated Power series of corresponding order.

# Warning

May only be used after having set the variables of your system with the @set_variables macro.

# Example
```jldoctest

julia> @set_variables x y
2-element Vector{Num}:
 x
 y

julia> @set_trunc_order 3
10×2 Matrix{Float64}:
 0.0  0.0
 1.0  0.0
 0.0  1.0
 2.0  0.0
 1.0  1.0
 0.0  2.0
 3.0  0.0
 2.0  1.0
 1.0  2.0
 0.0  3.0
```
"""
macro set_trunc_order(N::Int)
    v = size(Γcoord)[1]
    global trunc_order = N
    global M = tps_coef(N, v)
end

include("BaseOperators.jl")

include("TPSA.jl")

include("poisson.jl")

include("MapExp.jl")


function gen_matrix(f::Num, exp_order::Int)
    f_lin = pvt_tps_expr(f, 2)
    exp = gen_nl_map(f_lin, exp_order)
    map = exp(Γcoord)
    dim = length(Γcoord)
    Map = Matrix{Float64}(undef, dim, dim)
    for i in 1:dim
        d = (map[i]+1).val.dict     # +1 is a hacky trick to have map[i] be a sum of Num's, hence giving map[i].val a dict field
        for j in 1:dim
            Map[i,j] = Γcoord[j] in keys(d) ? convert(Float64, d[Γcoord[j]]) : 0
        end
    end
    return Map
end

end
