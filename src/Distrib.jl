# Author:    Matthieu Melennec
# Created:   22 September 2022
# Updated:   22 September 2022

abstract type Distrib end

"""
$(SIGNATURES)

Function to generate N datapoints following distribution D.

# Examples
```jldoctest

julia> D = Gaussian([0, 0], [1 0; 0 1])
Gaussian([0, 0], [1 0; 0 1])

julia> N = 10
10

julia> genDistrib(D, N)
2×10 Matrix{Float64}:
 -0.785693  -0.441129   1.39353  0.280005  1.45504   -0.0668804  -1.87176    0.975209  -1.31021   1.17989
  0.228701   0.570811  -1.22331  1.80514   0.797596   0.268831   -0.411179  -0.429013   0.576583  0.886648
```
"""
function genDistrib(D::Distrib, N::Int) end

"""
$(TYPEDEF)

Gaussian instance of a distribution with mean ``μ`` and covariance matrix ``Σ``.
"""
struct Gaussian <: Distrib
    μ::Vector
    Σ::Matrix
end

genDistrib(GD::Gaussian, N::Int) = rand(MvNormal(GD.μ, GD.Σ), N)
