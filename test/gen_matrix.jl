using Pkg
Pkg.activate("JuliAccel.jl")
using JuliAccel

@set_variables x px y py z δ
@set_trunc_order 6

ekin = 100e6  # eV
particle = Proton()

N = 1000    # Number of Particles in the bunch

L = 1.0     # Length of the dipole

beam = Beam(particle, ekin)

γ = get_gamma(beam)
β0 = get_beta(γ)

f = L/(γ * β0)^2

R = [1 L 0 0 0 0
    0 1 0 0 0 0
    0 0 1 L 0 0
    0 0 0 1 0 0
    0 0 0 0 1 f
    0 0 0 0 0 1]

display(R)

H = δ/β0 - sqrt((1/β0 + δ)^2 - px^2 - py^2 - f)

display(gen_matrix(-L*H))