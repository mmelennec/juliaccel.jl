using Pkg

deps = [
    "Random",
    "Distributions",
    "PhysicalConstants",
    "NaturallyUnitful",
    "DocStringExtensions",
    "LaTeXStrings",
    "Symbolics",
    "Reexport",
    "LinearAlgebra"
]

Pkg.add(deps)
